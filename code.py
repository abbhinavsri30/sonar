# -*- coding: utf-8 -*-
"""
Created on Sun Sep 24 20:26:16 2017

@author: Abhinav
"""

# Load libraries
import numpy
from matplotlib import pyplot
from pandas import read_csv
from pandas import set_option
from pandas.plotting import scatter_matrix
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier

# Load dataset
url = 'sonar.csv'
dataset = read_csv(url, header=None)

# shape
print(dataset.shape)

# types
set_option('display.max_rows', 500)
print(dataset.dtypes)

# head
set_option('display.width', 100)
print(dataset.head(20))

#descriptions, change precision to 3 places, SUMMARY
set_option('precision', 3)
print(dataset.describe())

#Again, as we expect, the data has the same range, but interestingly diﬀering mean values.
#There may be some beneﬁt from standardizing the data.

# class distribution
print(dataset.groupby(60).size())

#Unimodal Data Visualizations

# histograms
dataset.hist(sharex=False, sharey=False, xlabelsize=1, ylabelsize=1)
pyplot.show()  #We can see that there are a lot of Gaussian-like distributions and perhaps some exponential like distributions for other attributes.

# density
dataset.plot(kind='density', subplots=True, layout=(8,8), sharex=False, legend=False,
fontsize=1)
pyplot.show() #This is useful, you can see that many of the attributes have a skewed distribution. A power transform like a Box-Cox transform that can correct for the skew in distributions might be useful.

#Multimodal Data Visualizations

# correlation matrix
fig = pyplot.figure()
ax = fig.add_subplot(111)
cax = ax.matshow(dataset.corr(), vmin=-1, vmax=1, interpolation='none')
fig.colorbar(cax)
pyplot.show()

#It looks like there is also some structure in the order of the attributes. The red around
#the diagonal suggests that attributes that are next to each other are generally more correlated
#with each other. The blue patches also suggest some moderate negative correlation the further
#attributes are away from each other in the ordering. This makes sense if the order of the
#attributes refers to the angle of sensors for the sonar chirp.

#Validation Dataset

# Split-out validation dataset
array = dataset.values
X = array[:,0:60].astype(float)
Y = array[:,60]
validation_size = 0.20
seed = 7
X_train, X_validation, Y_train, Y_validation = train_test_split(X, Y,
test_size=validation_size, random_state=seed)

#Evaluate Algorithms: Baseline

#We don’t know what algorithms will do well on this dataset. Gut feel suggests distance based
#algorithms like k-Nearest Neighbors and Support Vector Machines may do well. Let’s design
#our test harness. We will use 10-fold cross-validation. The dataset is not too small and this is
#a good standard test harness conﬁguration. We will evaluate algorithms using the accuracy
#metric. This is a gross metric that will give a quick idea of how correct a given model is. More
#useful on binary classiﬁcation problems like this one.

# Test options and evaluation metric
num_folds = 10
seed = 7
scoring = 'accuracy'

#Let’s create a baseline of performance on this problem and spot-check a number of diﬀerent
#algorithms. We will select a suite of diﬀerent algorithms capable of working on this classiﬁcation
#problem. The six algorithms selected include:
# Linear Algorithms: Logistic Regression (LR) and Linear Discriminant Analysis (LDA).
# Nonlinear Algorithms: Classiﬁcation and Regression Trees (CART), Support Vector
#Machines (SVM), Gaussian Naive Bayes (NB) and k-Nearest Neighbors (KNN).

# Spot-Check Algorithms
models = []
models.append(('LR', LogisticRegression()))
models.append(('LDA', LinearDiscriminantAnalysis()))
models.append(('KNN', KNeighborsClassifier()))
models.append(('CART', DecisionTreeClassifier()))
models.append(('NB', GaussianNB()))
models.append(('SVM', SVC()))

#The algorithms all use default tuning parameters. Let’s compare the algorithms. We will
#display the mean and standard deviation of accuracy for each algorithm as we calculate it and
#collect the results for use later.

results = []
names = []
for name, model in models:
    kfold = KFold(n_splits=num_folds, random_state=seed)
cv_results = cross_val_score(model, X_train, Y_train, cv=kfold, scoring=scoring)
results.append(cv_results)
names.append(name)
msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
print(msg)

#The results suggest That both Logistic Regression and k-Nearest Neighbors may be worth further study.

#These are just mean accuracy values. It is always wise to look at the distribution of accuracy
#values calculated across cross-validation folds. We can do that graphically using box and whisker
#plots.

# Compare Algorithms
fig = pyplot.figure()
fig.suptitle('Algorithm Comparison')
ax = fig.add_subplot(111)
pyplot.boxplot(results)
ax.set_xticklabels(names)
pyplot.show()

#The results show a tight distribution for KNN which is encouraging, suggesting low variance.
#The poor results for SVM are surprising.

